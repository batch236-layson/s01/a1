//A package in java is used to group related classes. Think of it as a folder and a file directory.

//Package creation follows the "reverse name notation" for the naming convention
package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
        //Variable
        //Syntax : dataType identifier
        int age;
        char middleInitial;

        //variable declaration vs initialization
        int x;
        int y = 0;

        //initialization after declaration
        x = 1;
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        //Primitive data types
            //predefined within the Java Programming language which is used for "single-valued" variables with limited capabilities

        // int - whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);


        //long
        // L is added at the end of the number to be recognized as long
        long worldPopulation = 8000000000000L;
        System.out.println(worldPopulation);

        //float
        //f is added at the end of the number to recognized as float
        float pifloat = 3.141592265359f;
        System.out.println(pifloat);

        //double - floating point values
        double doubleFloat = 3.141592265359;
        System.out.println(doubleFloat);

        //char - single character
        //uses single quotes
        char letter = 'a';
        System.out.println(letter);

        //boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        //constants
        //java uses the "final" keyword to the variable's value that can't be changed
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //Non - primitive data type
            // also known as preference data types refer to instances or objects
            //do not directly store the value of a variable but rather remembers the reference to that variable

        //string
            //Stores a sequence or array of characters
            // String are actually object that can use methods
        String userName = "JSmith";
        System.out.println(userName);

        //Sample String Method
        int stringLength = userName.length();
        System.out.println(stringLength);
    }
}
